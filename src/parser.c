#include <tree_sitter/parser.h>

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#ifdef _MSC_VER
#pragma optimize("", off)
#elif defined(__clang__)
#pragma clang optimize off
#elif defined(__GNUC__)
#pragma GCC optimize ("O0")
#endif

#define LANGUAGE_VERSION 14
#define STATE_COUNT 67
#define LARGE_STATE_COUNT 2
#define SYMBOL_COUNT 71
#define ALIAS_COUNT 0
#define TOKEN_COUNT 42
#define EXTERNAL_TOKEN_COUNT 0
#define FIELD_COUNT 2
#define MAX_ALIAS_SEQUENCE_LENGTH 5
#define PRODUCTION_ID_COUNT 7

enum {
  anon_sym_extern = 1,
  anon_sym_suite = 2,
  anon_sym_endsuite = 3,
  anon_sym_defstatus = 4,
  anon_sym_edit = 5,
  anon_sym_family = 6,
  anon_sym_endfamily = 7,
  anon_sym_event = 8,
  anon_sym_repeat = 9,
  anon_sym_task = 10,
  anon_sym_trigger = 11,
  anon_sym_date = 12,
  anon_sym_DOT = 13,
  anon_sym_cron = 14,
  anon_sym_CLEAREVENT = 15,
  anon_sym_COMMAND = 16,
  anon_sym_DOC_PATH = 17,
  anon_sym_ECF_HOME = 18,
  anon_sym_ECF_INCLUDE = 19,
  anon_sym_MODE = 20,
  anon_sym_NODE = 21,
  anon_sym_OWNER = 22,
  anon_sym_PARM = 23,
  anon_sym_REQUEUEONCOMPLETE = 24,
  anon_sym_REQUEUEONERROR = 25,
  anon_sym_SCRIPT_NAME = 26,
  anon_sym_SCRIPT_PATH = 27,
  anon_sym_SETEVENT = 28,
  anon_sym_TASK_TIMEOUT = 29,
  anon_sym_TASK_TYPE = 30,
  sym_identifier = 31,
  sym_string_type = 32,
  sym_cron_option = 33,
  sym_time_type = 34,
  anon_sym_and = 35,
  anon_sym_or = 36,
  anon_sym_LT = 37,
  anon_sym_EQ_EQ = 38,
  anon_sym_GT = 39,
  sym_number = 40,
  sym_comment = 41,
  sym_source_file = 42,
  sym__definition = 43,
  sym_extern_statement = 44,
  sym_suite_definition = 45,
  sym__suite_block = 46,
  sym_defstatus_statement = 47,
  sym_edit_statement = 48,
  sym_family_definition = 49,
  sym__family_preamble = 50,
  sym__family_block = 51,
  sym_event_statement = 52,
  sym_repeat_statement = 53,
  sym_task_definition = 54,
  sym__task_block = 55,
  sym_trigger_statement = 56,
  sym_date_statement = 57,
  sym_cron_statement = 58,
  sym_edit_type = 59,
  sym__expression = 60,
  sym_boolean_operator = 61,
  sym_comparison_operator = 62,
  aux_sym_source_file_repeat1 = 63,
  aux_sym_suite_definition_repeat1 = 64,
  aux_sym_edit_statement_repeat1 = 65,
  aux_sym_family_definition_repeat1 = 66,
  aux_sym_family_definition_repeat2 = 67,
  aux_sym_task_definition_repeat1 = 68,
  aux_sym_date_statement_repeat1 = 69,
  aux_sym_cron_statement_repeat1 = 70,
};

static const char * const ts_symbol_names[] = {
  [ts_builtin_sym_end] = "end",
  [anon_sym_extern] = "extern",
  [anon_sym_suite] = "suite",
  [anon_sym_endsuite] = "endsuite",
  [anon_sym_defstatus] = "defstatus",
  [anon_sym_edit] = "edit",
  [anon_sym_family] = "family",
  [anon_sym_endfamily] = "endfamily",
  [anon_sym_event] = "event",
  [anon_sym_repeat] = "repeat",
  [anon_sym_task] = "task",
  [anon_sym_trigger] = "trigger",
  [anon_sym_date] = "date",
  [anon_sym_DOT] = ".",
  [anon_sym_cron] = "cron",
  [anon_sym_CLEAREVENT] = "CLEAREVENT",
  [anon_sym_COMMAND] = "COMMAND",
  [anon_sym_DOC_PATH] = "DOC_PATH",
  [anon_sym_ECF_HOME] = "ECF_HOME",
  [anon_sym_ECF_INCLUDE] = "ECF_INCLUDE",
  [anon_sym_MODE] = "MODE",
  [anon_sym_NODE] = "NODE",
  [anon_sym_OWNER] = "OWNER",
  [anon_sym_PARM] = "PARM",
  [anon_sym_REQUEUEONCOMPLETE] = "REQUEUEONCOMPLETE",
  [anon_sym_REQUEUEONERROR] = "REQUEUEONERROR",
  [anon_sym_SCRIPT_NAME] = "SCRIPT_NAME",
  [anon_sym_SCRIPT_PATH] = "SCRIPT_PATH",
  [anon_sym_SETEVENT] = "SETEVENT",
  [anon_sym_TASK_TIMEOUT] = "TASK_TIMEOUT",
  [anon_sym_TASK_TYPE] = "TASK_TYPE",
  [sym_identifier] = "identifier",
  [sym_string_type] = "string_type",
  [sym_cron_option] = "cron_option",
  [sym_time_type] = "time_type",
  [anon_sym_and] = "and",
  [anon_sym_or] = "or",
  [anon_sym_LT] = "<",
  [anon_sym_EQ_EQ] = "==",
  [anon_sym_GT] = ">",
  [sym_number] = "number",
  [sym_comment] = "comment",
  [sym_source_file] = "source_file",
  [sym__definition] = "_definition",
  [sym_extern_statement] = "extern_statement",
  [sym_suite_definition] = "suite_definition",
  [sym__suite_block] = "_suite_block",
  [sym_defstatus_statement] = "defstatus_statement",
  [sym_edit_statement] = "edit_statement",
  [sym_family_definition] = "family_definition",
  [sym__family_preamble] = "_family_preamble",
  [sym__family_block] = "_family_block",
  [sym_event_statement] = "event_statement",
  [sym_repeat_statement] = "repeat_statement",
  [sym_task_definition] = "task_definition",
  [sym__task_block] = "_task_block",
  [sym_trigger_statement] = "trigger_statement",
  [sym_date_statement] = "date_statement",
  [sym_cron_statement] = "cron_statement",
  [sym_edit_type] = "edit_type",
  [sym__expression] = "_expression",
  [sym_boolean_operator] = "boolean_operator",
  [sym_comparison_operator] = "comparison_operator",
  [aux_sym_source_file_repeat1] = "source_file_repeat1",
  [aux_sym_suite_definition_repeat1] = "suite_definition_repeat1",
  [aux_sym_edit_statement_repeat1] = "edit_statement_repeat1",
  [aux_sym_family_definition_repeat1] = "family_definition_repeat1",
  [aux_sym_family_definition_repeat2] = "family_definition_repeat2",
  [aux_sym_task_definition_repeat1] = "task_definition_repeat1",
  [aux_sym_date_statement_repeat1] = "date_statement_repeat1",
  [aux_sym_cron_statement_repeat1] = "cron_statement_repeat1",
};

static const TSSymbol ts_symbol_map[] = {
  [ts_builtin_sym_end] = ts_builtin_sym_end,
  [anon_sym_extern] = anon_sym_extern,
  [anon_sym_suite] = anon_sym_suite,
  [anon_sym_endsuite] = anon_sym_endsuite,
  [anon_sym_defstatus] = anon_sym_defstatus,
  [anon_sym_edit] = anon_sym_edit,
  [anon_sym_family] = anon_sym_family,
  [anon_sym_endfamily] = anon_sym_endfamily,
  [anon_sym_event] = anon_sym_event,
  [anon_sym_repeat] = anon_sym_repeat,
  [anon_sym_task] = anon_sym_task,
  [anon_sym_trigger] = anon_sym_trigger,
  [anon_sym_date] = anon_sym_date,
  [anon_sym_DOT] = anon_sym_DOT,
  [anon_sym_cron] = anon_sym_cron,
  [anon_sym_CLEAREVENT] = anon_sym_CLEAREVENT,
  [anon_sym_COMMAND] = anon_sym_COMMAND,
  [anon_sym_DOC_PATH] = anon_sym_DOC_PATH,
  [anon_sym_ECF_HOME] = anon_sym_ECF_HOME,
  [anon_sym_ECF_INCLUDE] = anon_sym_ECF_INCLUDE,
  [anon_sym_MODE] = anon_sym_MODE,
  [anon_sym_NODE] = anon_sym_NODE,
  [anon_sym_OWNER] = anon_sym_OWNER,
  [anon_sym_PARM] = anon_sym_PARM,
  [anon_sym_REQUEUEONCOMPLETE] = anon_sym_REQUEUEONCOMPLETE,
  [anon_sym_REQUEUEONERROR] = anon_sym_REQUEUEONERROR,
  [anon_sym_SCRIPT_NAME] = anon_sym_SCRIPT_NAME,
  [anon_sym_SCRIPT_PATH] = anon_sym_SCRIPT_PATH,
  [anon_sym_SETEVENT] = anon_sym_SETEVENT,
  [anon_sym_TASK_TIMEOUT] = anon_sym_TASK_TIMEOUT,
  [anon_sym_TASK_TYPE] = anon_sym_TASK_TYPE,
  [sym_identifier] = sym_identifier,
  [sym_string_type] = sym_string_type,
  [sym_cron_option] = sym_cron_option,
  [sym_time_type] = sym_time_type,
  [anon_sym_and] = anon_sym_and,
  [anon_sym_or] = anon_sym_or,
  [anon_sym_LT] = anon_sym_LT,
  [anon_sym_EQ_EQ] = anon_sym_EQ_EQ,
  [anon_sym_GT] = anon_sym_GT,
  [sym_number] = sym_number,
  [sym_comment] = sym_comment,
  [sym_source_file] = sym_source_file,
  [sym__definition] = sym__definition,
  [sym_extern_statement] = sym_extern_statement,
  [sym_suite_definition] = sym_suite_definition,
  [sym__suite_block] = sym__suite_block,
  [sym_defstatus_statement] = sym_defstatus_statement,
  [sym_edit_statement] = sym_edit_statement,
  [sym_family_definition] = sym_family_definition,
  [sym__family_preamble] = sym__family_preamble,
  [sym__family_block] = sym__family_block,
  [sym_event_statement] = sym_event_statement,
  [sym_repeat_statement] = sym_repeat_statement,
  [sym_task_definition] = sym_task_definition,
  [sym__task_block] = sym__task_block,
  [sym_trigger_statement] = sym_trigger_statement,
  [sym_date_statement] = sym_date_statement,
  [sym_cron_statement] = sym_cron_statement,
  [sym_edit_type] = sym_edit_type,
  [sym__expression] = sym__expression,
  [sym_boolean_operator] = sym_boolean_operator,
  [sym_comparison_operator] = sym_comparison_operator,
  [aux_sym_source_file_repeat1] = aux_sym_source_file_repeat1,
  [aux_sym_suite_definition_repeat1] = aux_sym_suite_definition_repeat1,
  [aux_sym_edit_statement_repeat1] = aux_sym_edit_statement_repeat1,
  [aux_sym_family_definition_repeat1] = aux_sym_family_definition_repeat1,
  [aux_sym_family_definition_repeat2] = aux_sym_family_definition_repeat2,
  [aux_sym_task_definition_repeat1] = aux_sym_task_definition_repeat1,
  [aux_sym_date_statement_repeat1] = aux_sym_date_statement_repeat1,
  [aux_sym_cron_statement_repeat1] = aux_sym_cron_statement_repeat1,
};

static const TSSymbolMetadata ts_symbol_metadata[] = {
  [ts_builtin_sym_end] = {
    .visible = false,
    .named = true,
  },
  [anon_sym_extern] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_suite] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_endsuite] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_defstatus] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_edit] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_family] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_endfamily] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_event] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_repeat] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_task] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_trigger] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_date] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DOT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_cron] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_CLEAREVENT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_COMMAND] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DOC_PATH] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_ECF_HOME] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_ECF_INCLUDE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_MODE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_NODE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_OWNER] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_PARM] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_REQUEUEONCOMPLETE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_REQUEUEONERROR] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_SCRIPT_NAME] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_SCRIPT_PATH] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_SETEVENT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_TASK_TIMEOUT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_TASK_TYPE] = {
    .visible = true,
    .named = false,
  },
  [sym_identifier] = {
    .visible = true,
    .named = true,
  },
  [sym_string_type] = {
    .visible = true,
    .named = true,
  },
  [sym_cron_option] = {
    .visible = true,
    .named = true,
  },
  [sym_time_type] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_and] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_or] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_EQ_EQ] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_GT] = {
    .visible = true,
    .named = false,
  },
  [sym_number] = {
    .visible = true,
    .named = true,
  },
  [sym_comment] = {
    .visible = true,
    .named = true,
  },
  [sym_source_file] = {
    .visible = true,
    .named = true,
  },
  [sym__definition] = {
    .visible = false,
    .named = true,
  },
  [sym_extern_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_suite_definition] = {
    .visible = true,
    .named = true,
  },
  [sym__suite_block] = {
    .visible = false,
    .named = true,
  },
  [sym_defstatus_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_edit_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_family_definition] = {
    .visible = true,
    .named = true,
  },
  [sym__family_preamble] = {
    .visible = false,
    .named = true,
  },
  [sym__family_block] = {
    .visible = false,
    .named = true,
  },
  [sym_event_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_repeat_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_task_definition] = {
    .visible = true,
    .named = true,
  },
  [sym__task_block] = {
    .visible = false,
    .named = true,
  },
  [sym_trigger_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_date_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_cron_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_edit_type] = {
    .visible = true,
    .named = true,
  },
  [sym__expression] = {
    .visible = false,
    .named = true,
  },
  [sym_boolean_operator] = {
    .visible = true,
    .named = true,
  },
  [sym_comparison_operator] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_source_file_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_suite_definition_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_edit_statement_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_family_definition_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_family_definition_repeat2] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_task_definition_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_date_statement_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_cron_statement_repeat1] = {
    .visible = false,
    .named = false,
  },
};

enum {
  field_body = 1,
  field_preamble = 2,
};

static const char * const ts_field_names[] = {
  [0] = NULL,
  [field_body] = "body",
  [field_preamble] = "preamble",
};

static const TSFieldMapSlice ts_field_map_slices[PRODUCTION_ID_COUNT] = {
  [1] = {.index = 0, .length = 1},
  [2] = {.index = 1, .length = 1},
  [3] = {.index = 2, .length = 2},
  [4] = {.index = 4, .length = 1},
  [5] = {.index = 5, .length = 1},
  [6] = {.index = 6, .length = 2},
};

static const TSFieldMapEntry ts_field_map_entries[] = {
  [0] =
    {field_body, 0, .inherited = true},
  [1] =
    {field_body, 0},
  [2] =
    {field_body, 0, .inherited = true},
    {field_body, 1, .inherited = true},
  [4] =
    {field_body, 2},
  [5] =
    {field_preamble, 2},
  [6] =
    {field_body, 3},
    {field_preamble, 2},
};

static const TSSymbol ts_alias_sequences[PRODUCTION_ID_COUNT][MAX_ALIAS_SEQUENCE_LENGTH] = {
  [0] = {0},
};

static const uint16_t ts_non_terminal_alias_map[] = {
  0,
};

static const TSStateId ts_primary_state_ids[STATE_COUNT] = {
  [0] = 0,
  [1] = 1,
  [2] = 2,
  [3] = 3,
  [4] = 4,
  [5] = 5,
  [6] = 6,
  [7] = 2,
  [8] = 8,
  [9] = 5,
  [10] = 3,
  [11] = 11,
  [12] = 4,
  [13] = 11,
  [14] = 14,
  [15] = 15,
  [16] = 16,
  [17] = 16,
  [18] = 16,
  [19] = 2,
  [20] = 20,
  [21] = 4,
  [22] = 22,
  [23] = 3,
  [24] = 24,
  [25] = 25,
  [26] = 26,
  [27] = 25,
  [28] = 24,
  [29] = 26,
  [30] = 26,
  [31] = 25,
  [32] = 32,
  [33] = 33,
  [34] = 24,
  [35] = 35,
  [36] = 36,
  [37] = 37,
  [38] = 38,
  [39] = 39,
  [40] = 40,
  [41] = 41,
  [42] = 42,
  [43] = 43,
  [44] = 44,
  [45] = 45,
  [46] = 46,
  [47] = 47,
  [48] = 48,
  [49] = 49,
  [50] = 50,
  [51] = 51,
  [52] = 52,
  [53] = 53,
  [54] = 54,
  [55] = 55,
  [56] = 56,
  [57] = 57,
  [58] = 58,
  [59] = 59,
  [60] = 60,
  [61] = 61,
  [62] = 62,
  [63] = 63,
  [64] = 64,
  [65] = 65,
  [66] = 66,
};

static bool ts_lex(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (eof) ADVANCE(175);
      if (lookahead == '#') ADVANCE(402);
      if (lookahead == '-') ADVANCE(172);
      if (lookahead == '.') ADVANCE(198);
      if (lookahead == '<') ADVANCE(397);
      if (lookahead == '=') ADVANCE(8);
      if (lookahead == '>') ADVANCE(399);
      if (lookahead == 'C') ADVANCE(50);
      if (lookahead == 'D') ADVANCE(68);
      if (lookahead == 'E') ADVANCE(16);
      if (lookahead == 'M') ADVANCE(67);
      if (lookahead == 'N') ADVANCE(74);
      if (lookahead == 'O') ADVANCE(103);
      if (lookahead == 'P') ADVANCE(9);
      if (lookahead == 'R') ADVANCE(25);
      if (lookahead == 'S') ADVANCE(19);
      if (lookahead == 'T') ADVANCE(10);
      if (lookahead == 'a') ADVANCE(144);
      if (lookahead == 'c') ADVANCE(150);
      if (lookahead == 'd') ADVANCE(109);
      if (lookahead == 'e') ADVANCE(115);
      if (lookahead == 'f') ADVANCE(110);
      if (lookahead == 'o') ADVANCE(151);
      if (lookahead == 'r') ADVANCE(120);
      if (lookahead == 's') ADVANCE(166);
      if (lookahead == 't') ADVANCE(111);
      if (lookahead == '"' ||
          lookahead == '\'') ADVANCE(171);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == ' ') SKIP(0)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(400);
      END_STATE();
    case 1:
      if (lookahead == ' ') ADVANCE(174);
      END_STATE();
    case 2:
      if (lookahead == '#') ADVANCE(402);
      if (lookahead == '.') ADVANCE(198);
      if (lookahead == 'c') ADVANCE(150);
      if (lookahead == 'd') ADVANCE(108);
      if (lookahead == 'e') ADVANCE(116);
      if (lookahead == 'f') ADVANCE(110);
      if (lookahead == 't') ADVANCE(111);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == ' ') SKIP(2)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(401);
      END_STATE();
    case 3:
      if (lookahead == '#') ADVANCE(402);
      if (lookahead == '<') ADVANCE(397);
      if (lookahead == '=') ADVANCE(8);
      if (lookahead == '>') ADVANCE(399);
      if (lookahead == 'a') ADVANCE(365);
      if (lookahead == 'c') ADVANCE(372);
      if (lookahead == 'd') ADVANCE(335);
      if (lookahead == 'e') ADVANCE(339);
      if (lookahead == 'f') ADVANCE(332);
      if (lookahead == 'o') ADVANCE(370);
      if (lookahead == 't') ADVANCE(333);
      if (lookahead == '"' ||
          lookahead == '\'') ADVANCE(171);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == ' ') SKIP(3)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(400);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('b' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 4:
      if (lookahead == '#') ADVANCE(402);
      if (lookahead == '<') ADVANCE(397);
      if (lookahead == '=') ADVANCE(8);
      if (lookahead == '>') ADVANCE(399);
      if (lookahead == 'a') ADVANCE(365);
      if (lookahead == 'd') ADVANCE(350);
      if (lookahead == 'e') ADVANCE(340);
      if (lookahead == 'f') ADVANCE(332);
      if (lookahead == 'o') ADVANCE(370);
      if (lookahead == '"' ||
          lookahead == '\'') ADVANCE(171);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == ' ') SKIP(4)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(400);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('b' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 5:
      if (lookahead == '#') ADVANCE(402);
      if (lookahead == '<') ADVANCE(397);
      if (lookahead == '=') ADVANCE(8);
      if (lookahead == '>') ADVANCE(399);
      if (lookahead == 'a') ADVANCE(365);
      if (lookahead == 'e') ADVANCE(338);
      if (lookahead == 'f') ADVANCE(332);
      if (lookahead == 'o') ADVANCE(370);
      if (lookahead == 'r') ADVANCE(344);
      if (lookahead == 't') ADVANCE(333);
      if (lookahead == '"' ||
          lookahead == '\'') ADVANCE(171);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == ' ') SKIP(5)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(400);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('b' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 6:
      if (lookahead == '#') ADVANCE(402);
      if (lookahead == 'C') ADVANCE(274);
      if (lookahead == 'D') ADVANCE(292);
      if (lookahead == 'E') ADVANCE(240);
      if (lookahead == 'M') ADVANCE(291);
      if (lookahead == 'N') ADVANCE(298);
      if (lookahead == 'O') ADVANCE(327);
      if (lookahead == 'P') ADVANCE(233);
      if (lookahead == 'R') ADVANCE(249);
      if (lookahead == 'S') ADVANCE(243);
      if (lookahead == 'T') ADVANCE(234);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == ' ') SKIP(6)
      if (lookahead == '-' ||
          lookahead == '/' ||
          lookahead == ':' ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 7:
      if (lookahead == '#') ADVANCE(402);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == ' ') SKIP(7)
      if (lookahead == '-' ||
          lookahead == '/' ||
          lookahead == ':' ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 8:
      if (lookahead == '=') ADVANCE(398);
      END_STATE();
    case 9:
      if (lookahead == 'A') ADVANCE(83);
      END_STATE();
    case 10:
      if (lookahead == 'A') ADVANCE(87);
      END_STATE();
    case 11:
      if (lookahead == 'A') ADVANCE(61);
      END_STATE();
    case 12:
      if (lookahead == 'A') ADVANCE(86);
      END_STATE();
    case 13:
      if (lookahead == 'A') ADVANCE(89);
      END_STATE();
    case 14:
      if (lookahead == 'A') ADVANCE(93);
      END_STATE();
    case 15:
      if (lookahead == 'A') ADVANCE(59);
      END_STATE();
    case 16:
      if (lookahead == 'C') ADVANCE(43);
      END_STATE();
    case 17:
      if (lookahead == 'C') ADVANCE(104);
      END_STATE();
    case 18:
      if (lookahead == 'C') ADVANCE(51);
      END_STATE();
    case 19:
      if (lookahead == 'C') ADVANCE(80);
      if (lookahead == 'E') ADVANCE(95);
      END_STATE();
    case 20:
      if (lookahead == 'C') ADVANCE(71);
      if (lookahead == 'E') ADVANCE(85);
      END_STATE();
    case 21:
      if (lookahead == 'D') ADVANCE(203);
      END_STATE();
    case 22:
      if (lookahead == 'D') ADVANCE(26);
      END_STATE();
    case 23:
      if (lookahead == 'D') ADVANCE(27);
      END_STATE();
    case 24:
      if (lookahead == 'D') ADVANCE(31);
      END_STATE();
    case 25:
      if (lookahead == 'E') ADVANCE(79);
      END_STATE();
    case 26:
      if (lookahead == 'E') ADVANCE(211);
      END_STATE();
    case 27:
      if (lookahead == 'E') ADVANCE(213);
      END_STATE();
    case 28:
      if (lookahead == 'E') ADVANCE(101);
      END_STATE();
    case 29:
      if (lookahead == 'E') ADVANCE(207);
      END_STATE();
    case 30:
      if (lookahead == 'E') ADVANCE(231);
      END_STATE();
    case 31:
      if (lookahead == 'E') ADVANCE(209);
      END_STATE();
    case 32:
      if (lookahead == 'E') ADVANCE(223);
      END_STATE();
    case 33:
      if (lookahead == 'E') ADVANCE(219);
      END_STATE();
    case 34:
      if (lookahead == 'E') ADVANCE(72);
      END_STATE();
    case 35:
      if (lookahead == 'E') ADVANCE(65);
      END_STATE();
    case 36:
      if (lookahead == 'E') ADVANCE(81);
      END_STATE();
    case 37:
      if (lookahead == 'E') ADVANCE(69);
      END_STATE();
    case 38:
      if (lookahead == 'E') ADVANCE(12);
      END_STATE();
    case 39:
      if (lookahead == 'E') ADVANCE(96);
      END_STATE();
    case 40:
      if (lookahead == 'E') ADVANCE(99);
      END_STATE();
    case 41:
      if (lookahead == 'E') ADVANCE(66);
      END_STATE();
    case 42:
      if (lookahead == 'E') ADVANCE(102);
      END_STATE();
    case 43:
      if (lookahead == 'F') ADVANCE(105);
      END_STATE();
    case 44:
      if (lookahead == 'H') ADVANCE(205);
      END_STATE();
    case 45:
      if (lookahead == 'H') ADVANCE(225);
      END_STATE();
    case 46:
      if (lookahead == 'H') ADVANCE(70);
      if (lookahead == 'I') ADVANCE(62);
      END_STATE();
    case 47:
      if (lookahead == 'I') ADVANCE(75);
      END_STATE();
    case 48:
      if (lookahead == 'I') ADVANCE(58);
      if (lookahead == 'Y') ADVANCE(78);
      END_STATE();
    case 49:
      if (lookahead == 'K') ADVANCE(107);
      END_STATE();
    case 50:
      if (lookahead == 'L') ADVANCE(38);
      if (lookahead == 'O') ADVANCE(54);
      END_STATE();
    case 51:
      if (lookahead == 'L') ADVANCE(100);
      END_STATE();
    case 52:
      if (lookahead == 'L') ADVANCE(39);
      END_STATE();
    case 53:
      if (lookahead == 'M') ADVANCE(217);
      END_STATE();
    case 54:
      if (lookahead == 'M') ADVANCE(55);
      END_STATE();
    case 55:
      if (lookahead == 'M') ADVANCE(11);
      END_STATE();
    case 56:
      if (lookahead == 'M') ADVANCE(76);
      END_STATE();
    case 57:
      if (lookahead == 'M') ADVANCE(29);
      END_STATE();
    case 58:
      if (lookahead == 'M') ADVANCE(37);
      END_STATE();
    case 59:
      if (lookahead == 'M') ADVANCE(32);
      END_STATE();
    case 60:
      if (lookahead == 'N') ADVANCE(20);
      END_STATE();
    case 61:
      if (lookahead == 'N') ADVANCE(21);
      END_STATE();
    case 62:
      if (lookahead == 'N') ADVANCE(18);
      END_STATE();
    case 63:
      if (lookahead == 'N') ADVANCE(15);
      if (lookahead == 'P') ADVANCE(14);
      END_STATE();
    case 64:
      if (lookahead == 'N') ADVANCE(36);
      END_STATE();
    case 65:
      if (lookahead == 'N') ADVANCE(90);
      END_STATE();
    case 66:
      if (lookahead == 'N') ADVANCE(91);
      END_STATE();
    case 67:
      if (lookahead == 'O') ADVANCE(22);
      END_STATE();
    case 68:
      if (lookahead == 'O') ADVANCE(17);
      END_STATE();
    case 69:
      if (lookahead == 'O') ADVANCE(98);
      END_STATE();
    case 70:
      if (lookahead == 'O') ADVANCE(57);
      END_STATE();
    case 71:
      if (lookahead == 'O') ADVANCE(56);
      END_STATE();
    case 72:
      if (lookahead == 'O') ADVANCE(60);
      END_STATE();
    case 73:
      if (lookahead == 'O') ADVANCE(82);
      END_STATE();
    case 74:
      if (lookahead == 'O') ADVANCE(23);
      END_STATE();
    case 75:
      if (lookahead == 'P') ADVANCE(94);
      END_STATE();
    case 76:
      if (lookahead == 'P') ADVANCE(52);
      END_STATE();
    case 77:
      if (lookahead == 'P') ADVANCE(13);
      END_STATE();
    case 78:
      if (lookahead == 'P') ADVANCE(30);
      END_STATE();
    case 79:
      if (lookahead == 'Q') ADVANCE(97);
      END_STATE();
    case 80:
      if (lookahead == 'R') ADVANCE(47);
      END_STATE();
    case 81:
      if (lookahead == 'R') ADVANCE(215);
      END_STATE();
    case 82:
      if (lookahead == 'R') ADVANCE(221);
      END_STATE();
    case 83:
      if (lookahead == 'R') ADVANCE(53);
      END_STATE();
    case 84:
      if (lookahead == 'R') ADVANCE(73);
      END_STATE();
    case 85:
      if (lookahead == 'R') ADVANCE(84);
      END_STATE();
    case 86:
      if (lookahead == 'R') ADVANCE(42);
      END_STATE();
    case 87:
      if (lookahead == 'S') ADVANCE(49);
      END_STATE();
    case 88:
      if (lookahead == 'T') ADVANCE(48);
      END_STATE();
    case 89:
      if (lookahead == 'T') ADVANCE(44);
      END_STATE();
    case 90:
      if (lookahead == 'T') ADVANCE(227);
      END_STATE();
    case 91:
      if (lookahead == 'T') ADVANCE(201);
      END_STATE();
    case 92:
      if (lookahead == 'T') ADVANCE(229);
      END_STATE();
    case 93:
      if (lookahead == 'T') ADVANCE(45);
      END_STATE();
    case 94:
      if (lookahead == 'T') ADVANCE(106);
      END_STATE();
    case 95:
      if (lookahead == 'T') ADVANCE(28);
      END_STATE();
    case 96:
      if (lookahead == 'T') ADVANCE(33);
      END_STATE();
    case 97:
      if (lookahead == 'U') ADVANCE(40);
      END_STATE();
    case 98:
      if (lookahead == 'U') ADVANCE(92);
      END_STATE();
    case 99:
      if (lookahead == 'U') ADVANCE(34);
      END_STATE();
    case 100:
      if (lookahead == 'U') ADVANCE(24);
      END_STATE();
    case 101:
      if (lookahead == 'V') ADVANCE(35);
      END_STATE();
    case 102:
      if (lookahead == 'V') ADVANCE(41);
      END_STATE();
    case 103:
      if (lookahead == 'W') ADVANCE(64);
      END_STATE();
    case 104:
      if (lookahead == '_') ADVANCE(77);
      END_STATE();
    case 105:
      if (lookahead == '_') ADVANCE(46);
      END_STATE();
    case 106:
      if (lookahead == '_') ADVANCE(63);
      END_STATE();
    case 107:
      if (lookahead == '_') ADVANCE(88);
      END_STATE();
    case 108:
      if (lookahead == 'a') ADVANCE(161);
      END_STATE();
    case 109:
      if (lookahead == 'a') ADVANCE(161);
      if (lookahead == 'e') ADVANCE(128);
      END_STATE();
    case 110:
      if (lookahead == 'a') ADVANCE(142);
      END_STATE();
    case 111:
      if (lookahead == 'a') ADVANCE(154);
      if (lookahead == 'r') ADVANCE(133);
      END_STATE();
    case 112:
      if (lookahead == 'a') ADVANCE(159);
      END_STATE();
    case 113:
      if (lookahead == 'a') ADVANCE(160);
      END_STATE();
    case 114:
      if (lookahead == 'a') ADVANCE(143);
      END_STATE();
    case 115:
      if (lookahead == 'd') ADVANCE(135);
      if (lookahead == 'n') ADVANCE(118);
      if (lookahead == 'v') ADVANCE(125);
      if (lookahead == 'x') ADVANCE(162);
      END_STATE();
    case 116:
      if (lookahead == 'd') ADVANCE(135);
      if (lookahead == 'n') ADVANCE(119);
      END_STATE();
    case 117:
      if (lookahead == 'd') ADVANCE(393);
      END_STATE();
    case 118:
      if (lookahead == 'd') ADVANCE(130);
      END_STATE();
    case 119:
      if (lookahead == 'd') ADVANCE(129);
      END_STATE();
    case 120:
      if (lookahead == 'e') ADVANCE(149);
      END_STATE();
    case 121:
      if (lookahead == 'e') ADVANCE(196);
      END_STATE();
    case 122:
      if (lookahead == 'e') ADVANCE(177);
      END_STATE();
    case 123:
      if (lookahead == 'e') ADVANCE(178);
      END_STATE();
    case 124:
      if (lookahead == 'e') ADVANCE(112);
      END_STATE();
    case 125:
      if (lookahead == 'e') ADVANCE(147);
      END_STATE();
    case 126:
      if (lookahead == 'e') ADVANCE(153);
      END_STATE();
    case 127:
      if (lookahead == 'e') ADVANCE(152);
      END_STATE();
    case 128:
      if (lookahead == 'f') ADVANCE(156);
      END_STATE();
    case 129:
      if (lookahead == 'f') ADVANCE(114);
      END_STATE();
    case 130:
      if (lookahead == 'f') ADVANCE(114);
      if (lookahead == 's') ADVANCE(168);
      END_STATE();
    case 131:
      if (lookahead == 'g') ADVANCE(132);
      END_STATE();
    case 132:
      if (lookahead == 'g') ADVANCE(127);
      END_STATE();
    case 133:
      if (lookahead == 'i') ADVANCE(131);
      END_STATE();
    case 134:
      if (lookahead == 'i') ADVANCE(140);
      END_STATE();
    case 135:
      if (lookahead == 'i') ADVANCE(157);
      END_STATE();
    case 136:
      if (lookahead == 'i') ADVANCE(141);
      END_STATE();
    case 137:
      if (lookahead == 'i') ADVANCE(163);
      END_STATE();
    case 138:
      if (lookahead == 'i') ADVANCE(164);
      END_STATE();
    case 139:
      if (lookahead == 'k') ADVANCE(192);
      END_STATE();
    case 140:
      if (lookahead == 'l') ADVANCE(169);
      END_STATE();
    case 141:
      if (lookahead == 'l') ADVANCE(170);
      END_STATE();
    case 142:
      if (lookahead == 'm') ADVANCE(134);
      END_STATE();
    case 143:
      if (lookahead == 'm') ADVANCE(136);
      END_STATE();
    case 144:
      if (lookahead == 'n') ADVANCE(117);
      END_STATE();
    case 145:
      if (lookahead == 'n') ADVANCE(199);
      END_STATE();
    case 146:
      if (lookahead == 'n') ADVANCE(176);
      END_STATE();
    case 147:
      if (lookahead == 'n') ADVANCE(158);
      END_STATE();
    case 148:
      if (lookahead == 'o') ADVANCE(145);
      END_STATE();
    case 149:
      if (lookahead == 'p') ADVANCE(124);
      END_STATE();
    case 150:
      if (lookahead == 'r') ADVANCE(148);
      END_STATE();
    case 151:
      if (lookahead == 'r') ADVANCE(395);
      END_STATE();
    case 152:
      if (lookahead == 'r') ADVANCE(194);
      END_STATE();
    case 153:
      if (lookahead == 'r') ADVANCE(146);
      END_STATE();
    case 154:
      if (lookahead == 's') ADVANCE(139);
      END_STATE();
    case 155:
      if (lookahead == 's') ADVANCE(180);
      END_STATE();
    case 156:
      if (lookahead == 's') ADVANCE(165);
      END_STATE();
    case 157:
      if (lookahead == 't') ADVANCE(182);
      END_STATE();
    case 158:
      if (lookahead == 't') ADVANCE(188);
      END_STATE();
    case 159:
      if (lookahead == 't') ADVANCE(190);
      END_STATE();
    case 160:
      if (lookahead == 't') ADVANCE(167);
      END_STATE();
    case 161:
      if (lookahead == 't') ADVANCE(121);
      END_STATE();
    case 162:
      if (lookahead == 't') ADVANCE(126);
      END_STATE();
    case 163:
      if (lookahead == 't') ADVANCE(122);
      END_STATE();
    case 164:
      if (lookahead == 't') ADVANCE(123);
      END_STATE();
    case 165:
      if (lookahead == 't') ADVANCE(113);
      END_STATE();
    case 166:
      if (lookahead == 'u') ADVANCE(137);
      END_STATE();
    case 167:
      if (lookahead == 'u') ADVANCE(155);
      END_STATE();
    case 168:
      if (lookahead == 'u') ADVANCE(138);
      END_STATE();
    case 169:
      if (lookahead == 'y') ADVANCE(184);
      END_STATE();
    case 170:
      if (lookahead == 'y') ADVANCE(186);
      END_STATE();
    case 171:
      if (lookahead == '"' ||
          lookahead == '\'') ADVANCE(390);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(171);
      END_STATE();
    case 172:
      if (lookahead == 'd' ||
          lookahead == 'm' ||
          lookahead == 'w') ADVANCE(1);
      END_STATE();
    case 173:
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(392);
      END_STATE();
    case 174:
      if (lookahead == ',' ||
          ('0' <= lookahead && lookahead <= '9')) ADVANCE(391);
      END_STATE();
    case 175:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    case 176:
      ACCEPT_TOKEN(anon_sym_extern);
      END_STATE();
    case 177:
      ACCEPT_TOKEN(anon_sym_suite);
      END_STATE();
    case 178:
      ACCEPT_TOKEN(anon_sym_endsuite);
      END_STATE();
    case 179:
      ACCEPT_TOKEN(anon_sym_endsuite);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 180:
      ACCEPT_TOKEN(anon_sym_defstatus);
      END_STATE();
    case 181:
      ACCEPT_TOKEN(anon_sym_defstatus);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 182:
      ACCEPT_TOKEN(anon_sym_edit);
      END_STATE();
    case 183:
      ACCEPT_TOKEN(anon_sym_edit);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 184:
      ACCEPT_TOKEN(anon_sym_family);
      END_STATE();
    case 185:
      ACCEPT_TOKEN(anon_sym_family);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 186:
      ACCEPT_TOKEN(anon_sym_endfamily);
      END_STATE();
    case 187:
      ACCEPT_TOKEN(anon_sym_endfamily);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 188:
      ACCEPT_TOKEN(anon_sym_event);
      END_STATE();
    case 189:
      ACCEPT_TOKEN(anon_sym_event);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 190:
      ACCEPT_TOKEN(anon_sym_repeat);
      END_STATE();
    case 191:
      ACCEPT_TOKEN(anon_sym_repeat);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 192:
      ACCEPT_TOKEN(anon_sym_task);
      END_STATE();
    case 193:
      ACCEPT_TOKEN(anon_sym_task);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 194:
      ACCEPT_TOKEN(anon_sym_trigger);
      END_STATE();
    case 195:
      ACCEPT_TOKEN(anon_sym_trigger);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 196:
      ACCEPT_TOKEN(anon_sym_date);
      END_STATE();
    case 197:
      ACCEPT_TOKEN(anon_sym_date);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 198:
      ACCEPT_TOKEN(anon_sym_DOT);
      END_STATE();
    case 199:
      ACCEPT_TOKEN(anon_sym_cron);
      END_STATE();
    case 200:
      ACCEPT_TOKEN(anon_sym_cron);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 201:
      ACCEPT_TOKEN(anon_sym_CLEAREVENT);
      END_STATE();
    case 202:
      ACCEPT_TOKEN(anon_sym_CLEAREVENT);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 203:
      ACCEPT_TOKEN(anon_sym_COMMAND);
      END_STATE();
    case 204:
      ACCEPT_TOKEN(anon_sym_COMMAND);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 205:
      ACCEPT_TOKEN(anon_sym_DOC_PATH);
      END_STATE();
    case 206:
      ACCEPT_TOKEN(anon_sym_DOC_PATH);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 207:
      ACCEPT_TOKEN(anon_sym_ECF_HOME);
      END_STATE();
    case 208:
      ACCEPT_TOKEN(anon_sym_ECF_HOME);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 209:
      ACCEPT_TOKEN(anon_sym_ECF_INCLUDE);
      END_STATE();
    case 210:
      ACCEPT_TOKEN(anon_sym_ECF_INCLUDE);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 211:
      ACCEPT_TOKEN(anon_sym_MODE);
      END_STATE();
    case 212:
      ACCEPT_TOKEN(anon_sym_MODE);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 213:
      ACCEPT_TOKEN(anon_sym_NODE);
      END_STATE();
    case 214:
      ACCEPT_TOKEN(anon_sym_NODE);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 215:
      ACCEPT_TOKEN(anon_sym_OWNER);
      END_STATE();
    case 216:
      ACCEPT_TOKEN(anon_sym_OWNER);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 217:
      ACCEPT_TOKEN(anon_sym_PARM);
      END_STATE();
    case 218:
      ACCEPT_TOKEN(anon_sym_PARM);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 219:
      ACCEPT_TOKEN(anon_sym_REQUEUEONCOMPLETE);
      END_STATE();
    case 220:
      ACCEPT_TOKEN(anon_sym_REQUEUEONCOMPLETE);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 221:
      ACCEPT_TOKEN(anon_sym_REQUEUEONERROR);
      END_STATE();
    case 222:
      ACCEPT_TOKEN(anon_sym_REQUEUEONERROR);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 223:
      ACCEPT_TOKEN(anon_sym_SCRIPT_NAME);
      END_STATE();
    case 224:
      ACCEPT_TOKEN(anon_sym_SCRIPT_NAME);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 225:
      ACCEPT_TOKEN(anon_sym_SCRIPT_PATH);
      END_STATE();
    case 226:
      ACCEPT_TOKEN(anon_sym_SCRIPT_PATH);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 227:
      ACCEPT_TOKEN(anon_sym_SETEVENT);
      END_STATE();
    case 228:
      ACCEPT_TOKEN(anon_sym_SETEVENT);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 229:
      ACCEPT_TOKEN(anon_sym_TASK_TIMEOUT);
      END_STATE();
    case 230:
      ACCEPT_TOKEN(anon_sym_TASK_TIMEOUT);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 231:
      ACCEPT_TOKEN(anon_sym_TASK_TYPE);
      END_STATE();
    case 232:
      ACCEPT_TOKEN(anon_sym_TASK_TYPE);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 233:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'A') ADVANCE(307);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('B' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 234:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'A') ADVANCE(311);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('B' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 235:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'A') ADVANCE(285);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('B' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 236:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'A') ADVANCE(310);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('B' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 237:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'A') ADVANCE(313);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('B' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 238:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'A') ADVANCE(317);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('B' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 239:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'A') ADVANCE(283);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('B' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 240:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'C') ADVANCE(267);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 241:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'C') ADVANCE(328);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 242:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'C') ADVANCE(275);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 243:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'C') ADVANCE(304);
      if (lookahead == 'E') ADVANCE(319);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 244:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'C') ADVANCE(295);
      if (lookahead == 'E') ADVANCE(309);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 245:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'D') ADVANCE(204);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 246:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'D') ADVANCE(250);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 247:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'D') ADVANCE(251);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 248:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'D') ADVANCE(255);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 249:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(303);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 250:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(212);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 251:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(214);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 252:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(325);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 253:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(208);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 254:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(232);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 255:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(210);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 256:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(224);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 257:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(220);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 258:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(296);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 259:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(289);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 260:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(305);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 261:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(293);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 262:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(236);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 263:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(320);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 264:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(323);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 265:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(290);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 266:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'E') ADVANCE(326);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 267:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'F') ADVANCE(329);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 268:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'H') ADVANCE(206);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 269:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'H') ADVANCE(226);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 270:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'H') ADVANCE(294);
      if (lookahead == 'I') ADVANCE(286);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 271:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'I') ADVANCE(299);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 272:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'I') ADVANCE(282);
      if (lookahead == 'Y') ADVANCE(302);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 273:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'K') ADVANCE(331);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 274:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'L') ADVANCE(262);
      if (lookahead == 'O') ADVANCE(278);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 275:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'L') ADVANCE(324);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 276:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'L') ADVANCE(263);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 277:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'M') ADVANCE(218);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 278:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'M') ADVANCE(279);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 279:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'M') ADVANCE(235);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 280:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'M') ADVANCE(300);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 281:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'M') ADVANCE(253);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 282:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'M') ADVANCE(261);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 283:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'M') ADVANCE(256);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 284:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'N') ADVANCE(244);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 285:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'N') ADVANCE(245);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 286:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'N') ADVANCE(242);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 287:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'N') ADVANCE(239);
      if (lookahead == 'P') ADVANCE(238);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 288:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'N') ADVANCE(260);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 289:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'N') ADVANCE(314);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 290:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'N') ADVANCE(315);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 291:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'O') ADVANCE(246);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 292:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'O') ADVANCE(241);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 293:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'O') ADVANCE(322);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 294:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'O') ADVANCE(281);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 295:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'O') ADVANCE(280);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 296:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'O') ADVANCE(284);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 297:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'O') ADVANCE(306);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 298:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'O') ADVANCE(247);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 299:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'P') ADVANCE(318);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 300:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'P') ADVANCE(276);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 301:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'P') ADVANCE(237);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 302:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'P') ADVANCE(254);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 303:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'Q') ADVANCE(321);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 304:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'R') ADVANCE(271);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 305:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'R') ADVANCE(216);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 306:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'R') ADVANCE(222);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 307:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'R') ADVANCE(277);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 308:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'R') ADVANCE(297);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 309:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'R') ADVANCE(308);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 310:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'R') ADVANCE(266);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 311:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'S') ADVANCE(273);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 312:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'T') ADVANCE(272);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 313:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'T') ADVANCE(268);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 314:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'T') ADVANCE(228);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 315:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'T') ADVANCE(202);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 316:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'T') ADVANCE(230);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 317:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'T') ADVANCE(269);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 318:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'T') ADVANCE(330);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 319:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'T') ADVANCE(252);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 320:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'T') ADVANCE(257);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 321:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'U') ADVANCE(264);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 322:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'U') ADVANCE(316);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 323:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'U') ADVANCE(258);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 324:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'U') ADVANCE(248);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 325:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'V') ADVANCE(259);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 326:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'V') ADVANCE(265);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 327:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'W') ADVANCE(288);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 328:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == '_') ADVANCE(301);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 329:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == '_') ADVANCE(270);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 330:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == '_') ADVANCE(287);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 331:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == '_') ADVANCE(312);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 332:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(363);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('b' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 333:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(373);
      if (lookahead == 'r') ADVANCE(356);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('b' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 334:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(379);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('b' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 335:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(381);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('b' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 336:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(380);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('b' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 337:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(364);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('b' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 338:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'd') ADVANCE(355);
      if (lookahead == 'n') ADVANCE(342);
      if (lookahead == 'v') ADVANCE(347);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 339:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'd') ADVANCE(355);
      if (lookahead == 'n') ADVANCE(342);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 340:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'd') ADVANCE(355);
      if (lookahead == 'n') ADVANCE(343);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 341:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'd') ADVANCE(394);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 342:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'd') ADVANCE(352);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 343:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'd') ADVANCE(374);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 344:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(369);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 345:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(197);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 346:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(179);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 347:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(367);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 348:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(334);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 349:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(371);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 350:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(351);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 351:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'f') ADVANCE(376);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 352:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'f') ADVANCE(337);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 353:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'g') ADVANCE(354);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 354:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'g') ADVANCE(349);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 355:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'i') ADVANCE(377);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 356:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'i') ADVANCE(353);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 357:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'i') ADVANCE(361);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 358:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'i') ADVANCE(362);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 359:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'i') ADVANCE(382);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 360:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'k') ADVANCE(193);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 361:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(386);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 362:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(387);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 363:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'm') ADVANCE(357);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 364:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'm') ADVANCE(358);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 365:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'n') ADVANCE(341);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 366:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'n') ADVANCE(200);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 367:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'n') ADVANCE(378);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 368:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'o') ADVANCE(366);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 369:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'p') ADVANCE(348);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 370:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(396);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 371:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(195);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 372:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(368);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 373:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 's') ADVANCE(360);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 374:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 's') ADVANCE(385);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 375:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 's') ADVANCE(181);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 376:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 's') ADVANCE(383);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 377:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(183);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 378:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(189);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 379:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(191);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 380:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(384);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 381:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(345);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 382:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(346);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 383:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(336);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 384:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'u') ADVANCE(375);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 385:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'u') ADVANCE(359);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 386:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'y') ADVANCE(185);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 387:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'y') ADVANCE(187);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 388:
      ACCEPT_TOKEN(sym_identifier);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 389:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(389);
      END_STATE();
    case 390:
      ACCEPT_TOKEN(sym_string_type);
      if (lookahead == '"' ||
          lookahead == '\'') ADVANCE(390);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(171);
      END_STATE();
    case 391:
      ACCEPT_TOKEN(sym_cron_option);
      if (lookahead == ',' ||
          ('0' <= lookahead && lookahead <= '9')) ADVANCE(391);
      END_STATE();
    case 392:
      ACCEPT_TOKEN(sym_time_type);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(392);
      END_STATE();
    case 393:
      ACCEPT_TOKEN(anon_sym_and);
      END_STATE();
    case 394:
      ACCEPT_TOKEN(anon_sym_and);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 395:
      ACCEPT_TOKEN(anon_sym_or);
      END_STATE();
    case 396:
      ACCEPT_TOKEN(anon_sym_or);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(389);
      if (lookahead == '-' ||
          ('/' <= lookahead && lookahead <= ':') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(388);
      END_STATE();
    case 397:
      ACCEPT_TOKEN(anon_sym_LT);
      END_STATE();
    case 398:
      ACCEPT_TOKEN(anon_sym_EQ_EQ);
      END_STATE();
    case 399:
      ACCEPT_TOKEN(anon_sym_GT);
      END_STATE();
    case 400:
      ACCEPT_TOKEN(sym_number);
      if (lookahead == ':') ADVANCE(173);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(400);
      END_STATE();
    case 401:
      ACCEPT_TOKEN(sym_number);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(401);
      END_STATE();
    case 402:
      ACCEPT_TOKEN(sym_comment);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(402);
      END_STATE();
    default:
      return false;
  }
}

static const TSLexMode ts_lex_modes[STATE_COUNT] = {
  [0] = {.lex_state = 0},
  [1] = {.lex_state = 0},
  [2] = {.lex_state = 5},
  [3] = {.lex_state = 3},
  [4] = {.lex_state = 3},
  [5] = {.lex_state = 5},
  [6] = {.lex_state = 5},
  [7] = {.lex_state = 3},
  [8] = {.lex_state = 5},
  [9] = {.lex_state = 3},
  [10] = {.lex_state = 5},
  [11] = {.lex_state = 3},
  [12] = {.lex_state = 5},
  [13] = {.lex_state = 5},
  [14] = {.lex_state = 5},
  [15] = {.lex_state = 5},
  [16] = {.lex_state = 6},
  [17] = {.lex_state = 6},
  [18] = {.lex_state = 6},
  [19] = {.lex_state = 4},
  [20] = {.lex_state = 0},
  [21] = {.lex_state = 4},
  [22] = {.lex_state = 0},
  [23] = {.lex_state = 4},
  [24] = {.lex_state = 5},
  [25] = {.lex_state = 3},
  [26] = {.lex_state = 5},
  [27] = {.lex_state = 5},
  [28] = {.lex_state = 3},
  [29] = {.lex_state = 3},
  [30] = {.lex_state = 4},
  [31] = {.lex_state = 4},
  [32] = {.lex_state = 0},
  [33] = {.lex_state = 0},
  [34] = {.lex_state = 4},
  [35] = {.lex_state = 0},
  [36] = {.lex_state = 0},
  [37] = {.lex_state = 0},
  [38] = {.lex_state = 2},
  [39] = {.lex_state = 2},
  [40] = {.lex_state = 0},
  [41] = {.lex_state = 0},
  [42] = {.lex_state = 2},
  [43] = {.lex_state = 0},
  [44] = {.lex_state = 0},
  [45] = {.lex_state = 0},
  [46] = {.lex_state = 0},
  [47] = {.lex_state = 0},
  [48] = {.lex_state = 0},
  [49] = {.lex_state = 0},
  [50] = {.lex_state = 0},
  [51] = {.lex_state = 0},
  [52] = {.lex_state = 0},
  [53] = {.lex_state = 0},
  [54] = {.lex_state = 0},
  [55] = {.lex_state = 0},
  [56] = {.lex_state = 0},
  [57] = {.lex_state = 0},
  [58] = {.lex_state = 0},
  [59] = {.lex_state = 0},
  [60] = {.lex_state = 0},
  [61] = {.lex_state = 7},
  [62] = {.lex_state = 7},
  [63] = {.lex_state = 7},
  [64] = {.lex_state = 7},
  [65] = {.lex_state = 0},
  [66] = {.lex_state = 7},
};

static const uint16_t ts_parse_table[LARGE_STATE_COUNT][SYMBOL_COUNT] = {
  [0] = {
    [ts_builtin_sym_end] = ACTIONS(1),
    [anon_sym_extern] = ACTIONS(1),
    [anon_sym_suite] = ACTIONS(1),
    [anon_sym_endsuite] = ACTIONS(1),
    [anon_sym_defstatus] = ACTIONS(1),
    [anon_sym_edit] = ACTIONS(1),
    [anon_sym_family] = ACTIONS(1),
    [anon_sym_endfamily] = ACTIONS(1),
    [anon_sym_event] = ACTIONS(1),
    [anon_sym_repeat] = ACTIONS(1),
    [anon_sym_task] = ACTIONS(1),
    [anon_sym_trigger] = ACTIONS(1),
    [anon_sym_date] = ACTIONS(1),
    [anon_sym_DOT] = ACTIONS(1),
    [anon_sym_cron] = ACTIONS(1),
    [anon_sym_CLEAREVENT] = ACTIONS(1),
    [anon_sym_COMMAND] = ACTIONS(1),
    [anon_sym_DOC_PATH] = ACTIONS(1),
    [anon_sym_ECF_HOME] = ACTIONS(1),
    [anon_sym_ECF_INCLUDE] = ACTIONS(1),
    [anon_sym_MODE] = ACTIONS(1),
    [anon_sym_NODE] = ACTIONS(1),
    [anon_sym_OWNER] = ACTIONS(1),
    [anon_sym_PARM] = ACTIONS(1),
    [anon_sym_REQUEUEONCOMPLETE] = ACTIONS(1),
    [anon_sym_REQUEUEONERROR] = ACTIONS(1),
    [anon_sym_SCRIPT_NAME] = ACTIONS(1),
    [anon_sym_SCRIPT_PATH] = ACTIONS(1),
    [anon_sym_SETEVENT] = ACTIONS(1),
    [anon_sym_TASK_TIMEOUT] = ACTIONS(1),
    [anon_sym_TASK_TYPE] = ACTIONS(1),
    [sym_string_type] = ACTIONS(1),
    [sym_cron_option] = ACTIONS(1),
    [sym_time_type] = ACTIONS(1),
    [anon_sym_and] = ACTIONS(1),
    [anon_sym_or] = ACTIONS(1),
    [anon_sym_LT] = ACTIONS(1),
    [anon_sym_EQ_EQ] = ACTIONS(1),
    [anon_sym_GT] = ACTIONS(1),
    [sym_number] = ACTIONS(1),
    [sym_comment] = ACTIONS(3),
  },
  [1] = {
    [sym_source_file] = STATE(65),
    [sym__definition] = STATE(60),
    [sym_extern_statement] = STATE(60),
    [sym_suite_definition] = STATE(59),
    [aux_sym_source_file_repeat1] = STATE(50),
    [ts_builtin_sym_end] = ACTIONS(5),
    [anon_sym_extern] = ACTIONS(7),
    [anon_sym_suite] = ACTIONS(9),
    [sym_comment] = ACTIONS(3),
  },
};

static const uint16_t ts_small_parse_table[] = {
  [0] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 2,
      sym_identifier,
      sym_number,
    ACTIONS(15), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(17), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(19), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    STATE(12), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
    ACTIONS(11), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_event,
      anon_sym_repeat,
      anon_sym_task,
      anon_sym_trigger,
  [36] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(23), 2,
      sym_identifier,
      sym_number,
    ACTIONS(26), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(29), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(32), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    STATE(3), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
    ACTIONS(21), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
      anon_sym_trigger,
      anon_sym_date,
      anon_sym_cron,
  [72] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(37), 2,
      sym_identifier,
      sym_number,
    ACTIONS(39), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(41), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(43), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    STATE(3), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
    ACTIONS(35), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
      anon_sym_trigger,
      anon_sym_date,
      anon_sym_cron,
  [108] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(17), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(47), 2,
      sym_identifier,
      sym_number,
    ACTIONS(49), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(19), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    STATE(10), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
    ACTIONS(45), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_event,
      anon_sym_repeat,
      anon_sym_task,
      anon_sym_trigger,
  [144] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(17), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(47), 2,
      sym_identifier,
      sym_number,
    ACTIONS(49), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(19), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    STATE(10), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
    ACTIONS(51), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_event,
      anon_sym_repeat,
      anon_sym_task,
      anon_sym_trigger,
  [180] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(41), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(53), 2,
      sym_identifier,
      sym_number,
    ACTIONS(55), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(43), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    STATE(4), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
    ACTIONS(11), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
      anon_sym_trigger,
      anon_sym_date,
      anon_sym_cron,
  [216] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(17), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(47), 2,
      sym_identifier,
      sym_number,
    ACTIONS(49), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(19), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    STATE(10), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
    ACTIONS(57), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_event,
      anon_sym_repeat,
      anon_sym_task,
      anon_sym_trigger,
  [252] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(37), 2,
      sym_identifier,
      sym_number,
    ACTIONS(39), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(41), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(43), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    STATE(3), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
    ACTIONS(45), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
      anon_sym_trigger,
      anon_sym_date,
      anon_sym_cron,
  [288] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(59), 2,
      sym_identifier,
      sym_number,
    ACTIONS(62), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(65), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(68), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    STATE(10), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
    ACTIONS(21), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_event,
      anon_sym_repeat,
      anon_sym_task,
      anon_sym_trigger,
  [324] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(41), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(73), 2,
      sym_identifier,
      sym_number,
    ACTIONS(75), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(43), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    STATE(9), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
    ACTIONS(71), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
      anon_sym_trigger,
      anon_sym_date,
      anon_sym_cron,
  [360] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(17), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(47), 2,
      sym_identifier,
      sym_number,
    ACTIONS(49), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(19), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    STATE(10), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
    ACTIONS(35), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_event,
      anon_sym_repeat,
      anon_sym_task,
      anon_sym_trigger,
  [396] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(17), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(77), 2,
      sym_identifier,
      sym_number,
    ACTIONS(79), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(19), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    STATE(5), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
    ACTIONS(71), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_event,
      anon_sym_repeat,
      anon_sym_task,
      anon_sym_trigger,
  [432] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(17), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(83), 2,
      sym_identifier,
      sym_number,
    ACTIONS(85), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(19), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    STATE(6), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
    ACTIONS(81), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_event,
      anon_sym_repeat,
      anon_sym_task,
      anon_sym_trigger,
  [468] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(17), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(89), 2,
      sym_identifier,
      sym_number,
    ACTIONS(91), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(19), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    STATE(8), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
    ACTIONS(87), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_event,
      anon_sym_repeat,
      anon_sym_task,
      anon_sym_trigger,
  [504] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(95), 1,
      sym_identifier,
    STATE(7), 1,
      sym_edit_type,
    ACTIONS(93), 16,
      anon_sym_CLEAREVENT,
      anon_sym_COMMAND,
      anon_sym_DOC_PATH,
      anon_sym_ECF_HOME,
      anon_sym_ECF_INCLUDE,
      anon_sym_MODE,
      anon_sym_NODE,
      anon_sym_OWNER,
      anon_sym_PARM,
      anon_sym_REQUEUEONCOMPLETE,
      anon_sym_REQUEUEONERROR,
      anon_sym_SCRIPT_NAME,
      anon_sym_SCRIPT_PATH,
      anon_sym_SETEVENT,
      anon_sym_TASK_TIMEOUT,
      anon_sym_TASK_TYPE,
  [532] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(99), 1,
      sym_identifier,
    STATE(2), 1,
      sym_edit_type,
    ACTIONS(97), 16,
      anon_sym_CLEAREVENT,
      anon_sym_COMMAND,
      anon_sym_DOC_PATH,
      anon_sym_ECF_HOME,
      anon_sym_ECF_INCLUDE,
      anon_sym_MODE,
      anon_sym_NODE,
      anon_sym_OWNER,
      anon_sym_PARM,
      anon_sym_REQUEUEONCOMPLETE,
      anon_sym_REQUEUEONERROR,
      anon_sym_SCRIPT_NAME,
      anon_sym_SCRIPT_PATH,
      anon_sym_SETEVENT,
      anon_sym_TASK_TIMEOUT,
      anon_sym_TASK_TYPE,
  [560] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(103), 1,
      sym_identifier,
    STATE(19), 1,
      sym_edit_type,
    ACTIONS(101), 16,
      anon_sym_CLEAREVENT,
      anon_sym_COMMAND,
      anon_sym_DOC_PATH,
      anon_sym_ECF_HOME,
      anon_sym_ECF_INCLUDE,
      anon_sym_MODE,
      anon_sym_NODE,
      anon_sym_OWNER,
      anon_sym_PARM,
      anon_sym_REQUEUEONCOMPLETE,
      anon_sym_REQUEUEONERROR,
      anon_sym_SCRIPT_NAME,
      anon_sym_SCRIPT_PATH,
      anon_sym_SETEVENT,
      anon_sym_TASK_TIMEOUT,
      anon_sym_TASK_TYPE,
  [588] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(105), 2,
      sym_identifier,
      sym_number,
    ACTIONS(107), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(109), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(111), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    ACTIONS(11), 4,
      anon_sym_endsuite,
      anon_sym_defstatus,
      anon_sym_edit,
      anon_sym_family,
    STATE(21), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
  [621] = 10,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(113), 1,
      anon_sym_edit,
    ACTIONS(115), 1,
      anon_sym_family,
    ACTIONS(117), 1,
      anon_sym_endfamily,
    ACTIONS(119), 1,
      anon_sym_event,
    ACTIONS(121), 1,
      anon_sym_repeat,
    ACTIONS(123), 1,
      anon_sym_task,
    ACTIONS(125), 1,
      anon_sym_trigger,
    STATE(47), 4,
      sym_family_definition,
      sym__family_block,
      sym_task_definition,
      aux_sym_family_definition_repeat2,
    STATE(22), 6,
      sym_edit_statement,
      sym__family_preamble,
      sym_event_statement,
      sym_repeat_statement,
      sym_trigger_statement,
      aux_sym_family_definition_repeat1,
  [660] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(109), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(127), 2,
      sym_identifier,
      sym_number,
    ACTIONS(129), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(111), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    ACTIONS(35), 4,
      anon_sym_endsuite,
      anon_sym_defstatus,
      anon_sym_edit,
      anon_sym_family,
    STATE(23), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
  [693] = 10,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(113), 1,
      anon_sym_edit,
    ACTIONS(115), 1,
      anon_sym_family,
    ACTIONS(119), 1,
      anon_sym_event,
    ACTIONS(121), 1,
      anon_sym_repeat,
    ACTIONS(123), 1,
      anon_sym_task,
    ACTIONS(125), 1,
      anon_sym_trigger,
    ACTIONS(131), 1,
      anon_sym_endfamily,
    STATE(48), 4,
      sym_family_definition,
      sym__family_block,
      sym_task_definition,
      aux_sym_family_definition_repeat2,
    STATE(33), 6,
      sym_edit_statement,
      sym__family_preamble,
      sym_event_statement,
      sym_repeat_statement,
      sym_trigger_statement,
      aux_sym_family_definition_repeat1,
  [732] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(133), 2,
      sym_identifier,
      sym_number,
    ACTIONS(136), 2,
      sym_string_type,
      sym_time_type,
    ACTIONS(139), 2,
      anon_sym_and,
      anon_sym_or,
    ACTIONS(142), 3,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    ACTIONS(21), 4,
      anon_sym_endsuite,
      anon_sym_defstatus,
      anon_sym_edit,
      anon_sym_family,
    STATE(23), 4,
      sym__expression,
      sym_boolean_operator,
      sym_comparison_operator,
      aux_sym_edit_statement_repeat1,
  [765] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(147), 5,
      sym_string_type,
      sym_time_type,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    ACTIONS(145), 11,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_event,
      anon_sym_repeat,
      anon_sym_task,
      anon_sym_trigger,
      sym_identifier,
      anon_sym_and,
      anon_sym_or,
      sym_number,
  [789] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(151), 5,
      sym_string_type,
      sym_time_type,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    ACTIONS(149), 11,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
      anon_sym_trigger,
      anon_sym_date,
      anon_sym_cron,
      sym_identifier,
      anon_sym_and,
      anon_sym_or,
      sym_number,
  [813] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(155), 5,
      sym_string_type,
      sym_time_type,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    ACTIONS(153), 11,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_event,
      anon_sym_repeat,
      anon_sym_task,
      anon_sym_trigger,
      sym_identifier,
      anon_sym_and,
      anon_sym_or,
      sym_number,
  [837] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(151), 5,
      sym_string_type,
      sym_time_type,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    ACTIONS(149), 11,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_event,
      anon_sym_repeat,
      anon_sym_task,
      anon_sym_trigger,
      sym_identifier,
      anon_sym_and,
      anon_sym_or,
      sym_number,
  [861] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(147), 5,
      sym_string_type,
      sym_time_type,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    ACTIONS(145), 11,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
      anon_sym_trigger,
      anon_sym_date,
      anon_sym_cron,
      sym_identifier,
      anon_sym_and,
      anon_sym_or,
      sym_number,
  [885] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(155), 5,
      sym_string_type,
      sym_time_type,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    ACTIONS(153), 11,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
      anon_sym_trigger,
      anon_sym_date,
      anon_sym_cron,
      sym_identifier,
      anon_sym_and,
      anon_sym_or,
      sym_number,
  [909] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(155), 5,
      sym_string_type,
      sym_time_type,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    ACTIONS(153), 8,
      anon_sym_endsuite,
      anon_sym_defstatus,
      anon_sym_edit,
      anon_sym_family,
      sym_identifier,
      anon_sym_and,
      anon_sym_or,
      sym_number,
  [930] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(151), 5,
      sym_string_type,
      sym_time_type,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    ACTIONS(149), 8,
      anon_sym_endsuite,
      anon_sym_defstatus,
      anon_sym_edit,
      anon_sym_family,
      sym_identifier,
      anon_sym_and,
      anon_sym_or,
      sym_number,
  [951] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(157), 1,
      anon_sym_edit,
    ACTIONS(162), 1,
      anon_sym_trigger,
    ACTIONS(165), 1,
      anon_sym_date,
    ACTIONS(168), 1,
      anon_sym_cron,
    ACTIONS(160), 3,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
    STATE(32), 6,
      sym_edit_statement,
      sym__task_block,
      sym_trigger_statement,
      sym_date_statement,
      sym_cron_statement,
      aux_sym_task_definition_repeat1,
  [980] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(171), 1,
      anon_sym_edit,
    ACTIONS(176), 1,
      anon_sym_event,
    ACTIONS(179), 1,
      anon_sym_repeat,
    ACTIONS(182), 1,
      anon_sym_trigger,
    ACTIONS(174), 3,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
    STATE(33), 6,
      sym_edit_statement,
      sym__family_preamble,
      sym_event_statement,
      sym_repeat_statement,
      sym_trigger_statement,
      aux_sym_family_definition_repeat1,
  [1009] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(147), 5,
      sym_string_type,
      sym_time_type,
      anon_sym_LT,
      anon_sym_EQ_EQ,
      anon_sym_GT,
    ACTIONS(145), 8,
      anon_sym_endsuite,
      anon_sym_defstatus,
      anon_sym_edit,
      anon_sym_family,
      sym_identifier,
      anon_sym_and,
      anon_sym_or,
      sym_number,
  [1030] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(185), 1,
      anon_sym_edit,
    ACTIONS(189), 1,
      anon_sym_trigger,
    ACTIONS(191), 1,
      anon_sym_date,
    ACTIONS(193), 1,
      anon_sym_cron,
    ACTIONS(187), 3,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
    STATE(32), 6,
      sym_edit_statement,
      sym__task_block,
      sym_trigger_statement,
      sym_date_statement,
      sym_cron_statement,
      aux_sym_task_definition_repeat1,
  [1059] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(185), 1,
      anon_sym_edit,
    ACTIONS(189), 1,
      anon_sym_trigger,
    ACTIONS(191), 1,
      anon_sym_date,
    ACTIONS(193), 1,
      anon_sym_cron,
    ACTIONS(195), 3,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
    STATE(35), 6,
      sym_edit_statement,
      sym__task_block,
      sym_trigger_statement,
      sym_date_statement,
      sym_cron_statement,
      aux_sym_task_definition_repeat1,
  [1088] = 4,
    ACTIONS(3), 1,
      sym_comment,
    STATE(41), 1,
      aux_sym_cron_statement_repeat1,
    ACTIONS(199), 2,
      sym_cron_option,
      sym_time_type,
    ACTIONS(197), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
      anon_sym_trigger,
      anon_sym_date,
      anon_sym_cron,
  [1108] = 4,
    ACTIONS(3), 1,
      sym_comment,
    STATE(42), 1,
      aux_sym_date_statement_repeat1,
    ACTIONS(203), 2,
      anon_sym_DOT,
      sym_number,
    ACTIONS(201), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
      anon_sym_trigger,
      anon_sym_date,
      anon_sym_cron,
  [1128] = 4,
    ACTIONS(3), 1,
      sym_comment,
    STATE(38), 1,
      aux_sym_date_statement_repeat1,
    ACTIONS(207), 2,
      anon_sym_DOT,
      sym_number,
    ACTIONS(205), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
      anon_sym_trigger,
      anon_sym_date,
      anon_sym_cron,
  [1148] = 4,
    ACTIONS(3), 1,
      sym_comment,
    STATE(37), 1,
      aux_sym_cron_statement_repeat1,
    ACTIONS(211), 2,
      sym_cron_option,
      sym_time_type,
    ACTIONS(209), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
      anon_sym_trigger,
      anon_sym_date,
      anon_sym_cron,
  [1168] = 4,
    ACTIONS(3), 1,
      sym_comment,
    STATE(41), 1,
      aux_sym_cron_statement_repeat1,
    ACTIONS(215), 2,
      sym_cron_option,
      sym_time_type,
    ACTIONS(213), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
      anon_sym_trigger,
      anon_sym_date,
      anon_sym_cron,
  [1188] = 4,
    ACTIONS(3), 1,
      sym_comment,
    STATE(42), 1,
      aux_sym_date_statement_repeat1,
    ACTIONS(220), 2,
      anon_sym_DOT,
      sym_number,
    ACTIONS(218), 7,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
      anon_sym_trigger,
      anon_sym_date,
      anon_sym_cron,
  [1208] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(115), 1,
      anon_sym_family,
    ACTIONS(223), 1,
      anon_sym_endsuite,
    ACTIONS(225), 1,
      anon_sym_defstatus,
    ACTIONS(227), 1,
      anon_sym_edit,
    STATE(44), 5,
      sym__suite_block,
      sym_defstatus_statement,
      sym_edit_statement,
      sym_family_definition,
      aux_sym_suite_definition_repeat1,
  [1231] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(115), 1,
      anon_sym_family,
    ACTIONS(225), 1,
      anon_sym_defstatus,
    ACTIONS(227), 1,
      anon_sym_edit,
    ACTIONS(229), 1,
      anon_sym_endsuite,
    STATE(45), 5,
      sym__suite_block,
      sym_defstatus_statement,
      sym_edit_statement,
      sym_family_definition,
      aux_sym_suite_definition_repeat1,
  [1254] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(231), 1,
      anon_sym_endsuite,
    ACTIONS(233), 1,
      anon_sym_defstatus,
    ACTIONS(236), 1,
      anon_sym_edit,
    ACTIONS(239), 1,
      anon_sym_family,
    STATE(45), 5,
      sym__suite_block,
      sym_defstatus_statement,
      sym_edit_statement,
      sym_family_definition,
      aux_sym_suite_definition_repeat1,
  [1277] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(242), 1,
      ts_builtin_sym_end,
    ACTIONS(244), 1,
      anon_sym_extern,
    ACTIONS(247), 1,
      anon_sym_suite,
    STATE(46), 1,
      aux_sym_source_file_repeat1,
    STATE(59), 1,
      sym_suite_definition,
    STATE(60), 2,
      sym__definition,
      sym_extern_statement,
  [1300] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(115), 1,
      anon_sym_family,
    ACTIONS(123), 1,
      anon_sym_task,
    ACTIONS(250), 1,
      anon_sym_endfamily,
    STATE(49), 4,
      sym_family_definition,
      sym__family_block,
      sym_task_definition,
      aux_sym_family_definition_repeat2,
  [1319] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(115), 1,
      anon_sym_family,
    ACTIONS(123), 1,
      anon_sym_task,
    ACTIONS(252), 1,
      anon_sym_endfamily,
    STATE(49), 4,
      sym_family_definition,
      sym__family_block,
      sym_task_definition,
      aux_sym_family_definition_repeat2,
  [1338] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(254), 1,
      anon_sym_family,
    ACTIONS(257), 1,
      anon_sym_endfamily,
    ACTIONS(259), 1,
      anon_sym_task,
    STATE(49), 4,
      sym_family_definition,
      sym__family_block,
      sym_task_definition,
      aux_sym_family_definition_repeat2,
  [1357] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(7), 1,
      anon_sym_extern,
    ACTIONS(9), 1,
      anon_sym_suite,
    ACTIONS(262), 1,
      ts_builtin_sym_end,
    STATE(46), 1,
      aux_sym_source_file_repeat1,
    STATE(59), 1,
      sym_suite_definition,
    STATE(60), 2,
      sym__definition,
      sym_extern_statement,
  [1380] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(264), 6,
      anon_sym_endsuite,
      anon_sym_defstatus,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
  [1392] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(266), 6,
      anon_sym_endsuite,
      anon_sym_defstatus,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
  [1404] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(268), 6,
      anon_sym_endsuite,
      anon_sym_defstatus,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
  [1416] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(270), 6,
      anon_sym_endsuite,
      anon_sym_defstatus,
      anon_sym_edit,
      anon_sym_family,
      anon_sym_endfamily,
      anon_sym_task,
  [1428] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(272), 4,
      anon_sym_endsuite,
      anon_sym_defstatus,
      anon_sym_edit,
      anon_sym_family,
  [1438] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(274), 3,
      ts_builtin_sym_end,
      anon_sym_extern,
      anon_sym_suite,
  [1447] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(276), 3,
      ts_builtin_sym_end,
      anon_sym_extern,
      anon_sym_suite,
  [1456] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(278), 3,
      ts_builtin_sym_end,
      anon_sym_extern,
      anon_sym_suite,
  [1465] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(280), 3,
      ts_builtin_sym_end,
      anon_sym_extern,
      anon_sym_suite,
  [1474] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(282), 3,
      ts_builtin_sym_end,
      anon_sym_extern,
      anon_sym_suite,
  [1483] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(284), 1,
      sym_identifier,
  [1490] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(286), 1,
      sym_identifier,
  [1497] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(288), 1,
      sym_identifier,
  [1504] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(290), 1,
      sym_identifier,
  [1511] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(292), 1,
      ts_builtin_sym_end,
  [1518] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(294), 1,
      sym_identifier,
};

static const uint32_t ts_small_parse_table_map[] = {
  [SMALL_STATE(2)] = 0,
  [SMALL_STATE(3)] = 36,
  [SMALL_STATE(4)] = 72,
  [SMALL_STATE(5)] = 108,
  [SMALL_STATE(6)] = 144,
  [SMALL_STATE(7)] = 180,
  [SMALL_STATE(8)] = 216,
  [SMALL_STATE(9)] = 252,
  [SMALL_STATE(10)] = 288,
  [SMALL_STATE(11)] = 324,
  [SMALL_STATE(12)] = 360,
  [SMALL_STATE(13)] = 396,
  [SMALL_STATE(14)] = 432,
  [SMALL_STATE(15)] = 468,
  [SMALL_STATE(16)] = 504,
  [SMALL_STATE(17)] = 532,
  [SMALL_STATE(18)] = 560,
  [SMALL_STATE(19)] = 588,
  [SMALL_STATE(20)] = 621,
  [SMALL_STATE(21)] = 660,
  [SMALL_STATE(22)] = 693,
  [SMALL_STATE(23)] = 732,
  [SMALL_STATE(24)] = 765,
  [SMALL_STATE(25)] = 789,
  [SMALL_STATE(26)] = 813,
  [SMALL_STATE(27)] = 837,
  [SMALL_STATE(28)] = 861,
  [SMALL_STATE(29)] = 885,
  [SMALL_STATE(30)] = 909,
  [SMALL_STATE(31)] = 930,
  [SMALL_STATE(32)] = 951,
  [SMALL_STATE(33)] = 980,
  [SMALL_STATE(34)] = 1009,
  [SMALL_STATE(35)] = 1030,
  [SMALL_STATE(36)] = 1059,
  [SMALL_STATE(37)] = 1088,
  [SMALL_STATE(38)] = 1108,
  [SMALL_STATE(39)] = 1128,
  [SMALL_STATE(40)] = 1148,
  [SMALL_STATE(41)] = 1168,
  [SMALL_STATE(42)] = 1188,
  [SMALL_STATE(43)] = 1208,
  [SMALL_STATE(44)] = 1231,
  [SMALL_STATE(45)] = 1254,
  [SMALL_STATE(46)] = 1277,
  [SMALL_STATE(47)] = 1300,
  [SMALL_STATE(48)] = 1319,
  [SMALL_STATE(49)] = 1338,
  [SMALL_STATE(50)] = 1357,
  [SMALL_STATE(51)] = 1380,
  [SMALL_STATE(52)] = 1392,
  [SMALL_STATE(53)] = 1404,
  [SMALL_STATE(54)] = 1416,
  [SMALL_STATE(55)] = 1428,
  [SMALL_STATE(56)] = 1438,
  [SMALL_STATE(57)] = 1447,
  [SMALL_STATE(58)] = 1456,
  [SMALL_STATE(59)] = 1465,
  [SMALL_STATE(60)] = 1474,
  [SMALL_STATE(61)] = 1483,
  [SMALL_STATE(62)] = 1490,
  [SMALL_STATE(63)] = 1497,
  [SMALL_STATE(64)] = 1504,
  [SMALL_STATE(65)] = 1511,
  [SMALL_STATE(66)] = 1518,
};

static const TSParseActionEntry ts_parse_actions[] = {
  [0] = {.entry = {.count = 0, .reusable = false}},
  [1] = {.entry = {.count = 1, .reusable = false}}, RECOVER(),
  [3] = {.entry = {.count = 1, .reusable = true}}, SHIFT_EXTRA(),
  [5] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 0),
  [7] = {.entry = {.count = 1, .reusable = true}}, SHIFT(62),
  [9] = {.entry = {.count = 1, .reusable = true}}, SHIFT(66),
  [11] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_edit_statement, 2),
  [13] = {.entry = {.count = 1, .reusable = false}}, SHIFT(12),
  [15] = {.entry = {.count = 1, .reusable = true}}, SHIFT(12),
  [17] = {.entry = {.count = 1, .reusable = false}}, SHIFT(26),
  [19] = {.entry = {.count = 1, .reusable = true}}, SHIFT(27),
  [21] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_edit_statement_repeat1, 2),
  [23] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_edit_statement_repeat1, 2), SHIFT_REPEAT(3),
  [26] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_edit_statement_repeat1, 2), SHIFT_REPEAT(3),
  [29] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_edit_statement_repeat1, 2), SHIFT_REPEAT(29),
  [32] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_edit_statement_repeat1, 2), SHIFT_REPEAT(25),
  [35] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_edit_statement, 3),
  [37] = {.entry = {.count = 1, .reusable = false}}, SHIFT(3),
  [39] = {.entry = {.count = 1, .reusable = true}}, SHIFT(3),
  [41] = {.entry = {.count = 1, .reusable = false}}, SHIFT(29),
  [43] = {.entry = {.count = 1, .reusable = true}}, SHIFT(25),
  [45] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_trigger_statement, 2),
  [47] = {.entry = {.count = 1, .reusable = false}}, SHIFT(10),
  [49] = {.entry = {.count = 1, .reusable = true}}, SHIFT(10),
  [51] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_repeat_statement, 2),
  [53] = {.entry = {.count = 1, .reusable = false}}, SHIFT(4),
  [55] = {.entry = {.count = 1, .reusable = true}}, SHIFT(4),
  [57] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_event_statement, 2),
  [59] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_edit_statement_repeat1, 2), SHIFT_REPEAT(10),
  [62] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_edit_statement_repeat1, 2), SHIFT_REPEAT(10),
  [65] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_edit_statement_repeat1, 2), SHIFT_REPEAT(26),
  [68] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_edit_statement_repeat1, 2), SHIFT_REPEAT(27),
  [71] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_trigger_statement, 1),
  [73] = {.entry = {.count = 1, .reusable = false}}, SHIFT(9),
  [75] = {.entry = {.count = 1, .reusable = true}}, SHIFT(9),
  [77] = {.entry = {.count = 1, .reusable = false}}, SHIFT(5),
  [79] = {.entry = {.count = 1, .reusable = true}}, SHIFT(5),
  [81] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_repeat_statement, 1),
  [83] = {.entry = {.count = 1, .reusable = false}}, SHIFT(6),
  [85] = {.entry = {.count = 1, .reusable = true}}, SHIFT(6),
  [87] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_event_statement, 1),
  [89] = {.entry = {.count = 1, .reusable = false}}, SHIFT(8),
  [91] = {.entry = {.count = 1, .reusable = true}}, SHIFT(8),
  [93] = {.entry = {.count = 1, .reusable = false}}, SHIFT(28),
  [95] = {.entry = {.count = 1, .reusable = false}}, SHIFT(7),
  [97] = {.entry = {.count = 1, .reusable = false}}, SHIFT(24),
  [99] = {.entry = {.count = 1, .reusable = false}}, SHIFT(2),
  [101] = {.entry = {.count = 1, .reusable = false}}, SHIFT(34),
  [103] = {.entry = {.count = 1, .reusable = false}}, SHIFT(19),
  [105] = {.entry = {.count = 1, .reusable = false}}, SHIFT(21),
  [107] = {.entry = {.count = 1, .reusable = true}}, SHIFT(21),
  [109] = {.entry = {.count = 1, .reusable = false}}, SHIFT(30),
  [111] = {.entry = {.count = 1, .reusable = true}}, SHIFT(31),
  [113] = {.entry = {.count = 1, .reusable = true}}, SHIFT(17),
  [115] = {.entry = {.count = 1, .reusable = true}}, SHIFT(63),
  [117] = {.entry = {.count = 1, .reusable = true}}, SHIFT(52),
  [119] = {.entry = {.count = 1, .reusable = true}}, SHIFT(15),
  [121] = {.entry = {.count = 1, .reusable = true}}, SHIFT(14),
  [123] = {.entry = {.count = 1, .reusable = true}}, SHIFT(61),
  [125] = {.entry = {.count = 1, .reusable = true}}, SHIFT(13),
  [127] = {.entry = {.count = 1, .reusable = false}}, SHIFT(23),
  [129] = {.entry = {.count = 1, .reusable = true}}, SHIFT(23),
  [131] = {.entry = {.count = 1, .reusable = true}}, SHIFT(53),
  [133] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_edit_statement_repeat1, 2), SHIFT_REPEAT(23),
  [136] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_edit_statement_repeat1, 2), SHIFT_REPEAT(23),
  [139] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_edit_statement_repeat1, 2), SHIFT_REPEAT(30),
  [142] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_edit_statement_repeat1, 2), SHIFT_REPEAT(31),
  [145] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_edit_type, 1),
  [147] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_edit_type, 1),
  [149] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_comparison_operator, 1),
  [151] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_comparison_operator, 1),
  [153] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_boolean_operator, 1),
  [155] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_boolean_operator, 1),
  [157] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_task_definition_repeat1, 2), SHIFT_REPEAT(16),
  [160] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_task_definition_repeat1, 2),
  [162] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_task_definition_repeat1, 2), SHIFT_REPEAT(11),
  [165] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_task_definition_repeat1, 2), SHIFT_REPEAT(39),
  [168] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_task_definition_repeat1, 2), SHIFT_REPEAT(40),
  [171] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_family_definition_repeat1, 2), SHIFT_REPEAT(17),
  [174] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_family_definition_repeat1, 2),
  [176] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_family_definition_repeat1, 2), SHIFT_REPEAT(15),
  [179] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_family_definition_repeat1, 2), SHIFT_REPEAT(14),
  [182] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_family_definition_repeat1, 2), SHIFT_REPEAT(13),
  [185] = {.entry = {.count = 1, .reusable = true}}, SHIFT(16),
  [187] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_task_definition, 3, .production_id = 4),
  [189] = {.entry = {.count = 1, .reusable = true}}, SHIFT(11),
  [191] = {.entry = {.count = 1, .reusable = true}}, SHIFT(39),
  [193] = {.entry = {.count = 1, .reusable = true}}, SHIFT(40),
  [195] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_task_definition, 2),
  [197] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_cron_statement, 2),
  [199] = {.entry = {.count = 1, .reusable = true}}, SHIFT(41),
  [201] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_date_statement, 2),
  [203] = {.entry = {.count = 1, .reusable = true}}, SHIFT(42),
  [205] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_date_statement, 1),
  [207] = {.entry = {.count = 1, .reusable = true}}, SHIFT(38),
  [209] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_cron_statement, 1),
  [211] = {.entry = {.count = 1, .reusable = true}}, SHIFT(37),
  [213] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_cron_statement_repeat1, 2),
  [215] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_cron_statement_repeat1, 2), SHIFT_REPEAT(41),
  [218] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_date_statement_repeat1, 2),
  [220] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_date_statement_repeat1, 2), SHIFT_REPEAT(42),
  [223] = {.entry = {.count = 1, .reusable = true}}, SHIFT(57),
  [225] = {.entry = {.count = 1, .reusable = true}}, SHIFT(64),
  [227] = {.entry = {.count = 1, .reusable = true}}, SHIFT(18),
  [229] = {.entry = {.count = 1, .reusable = true}}, SHIFT(56),
  [231] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_suite_definition_repeat1, 2),
  [233] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_suite_definition_repeat1, 2), SHIFT_REPEAT(64),
  [236] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_suite_definition_repeat1, 2), SHIFT_REPEAT(18),
  [239] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_suite_definition_repeat1, 2), SHIFT_REPEAT(63),
  [242] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2, .production_id = 3),
  [244] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2, .production_id = 3), SHIFT_REPEAT(62),
  [247] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2, .production_id = 3), SHIFT_REPEAT(66),
  [250] = {.entry = {.count = 1, .reusable = true}}, SHIFT(54),
  [252] = {.entry = {.count = 1, .reusable = true}}, SHIFT(51),
  [254] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_family_definition_repeat2, 2), SHIFT_REPEAT(63),
  [257] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_family_definition_repeat2, 2),
  [259] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_family_definition_repeat2, 2), SHIFT_REPEAT(61),
  [262] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 1, .production_id = 1),
  [264] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_family_definition, 5, .production_id = 6),
  [266] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_family_definition, 3),
  [268] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_family_definition, 4, .production_id = 5),
  [270] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_family_definition, 4, .production_id = 4),
  [272] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_defstatus_statement, 2),
  [274] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_suite_definition, 4, .production_id = 4),
  [276] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_suite_definition, 3),
  [278] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_extern_statement, 2),
  [280] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__definition, 1, .production_id = 2),
  [282] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 1, .production_id = 1),
  [284] = {.entry = {.count = 1, .reusable = true}}, SHIFT(36),
  [286] = {.entry = {.count = 1, .reusable = true}}, SHIFT(58),
  [288] = {.entry = {.count = 1, .reusable = true}}, SHIFT(20),
  [290] = {.entry = {.count = 1, .reusable = true}}, SHIFT(55),
  [292] = {.entry = {.count = 1, .reusable = true}},  ACCEPT_INPUT(),
  [294] = {.entry = {.count = 1, .reusable = true}}, SHIFT(43),
};

#ifdef __cplusplus
extern "C" {
#endif
#ifdef _WIN32
#define extern __declspec(dllexport)
#endif

extern const TSLanguage *tree_sitter_ecflow_def(void) {
  static const TSLanguage language = {
    .version = LANGUAGE_VERSION,
    .symbol_count = SYMBOL_COUNT,
    .alias_count = ALIAS_COUNT,
    .token_count = TOKEN_COUNT,
    .external_token_count = EXTERNAL_TOKEN_COUNT,
    .state_count = STATE_COUNT,
    .large_state_count = LARGE_STATE_COUNT,
    .production_id_count = PRODUCTION_ID_COUNT,
    .field_count = FIELD_COUNT,
    .max_alias_sequence_length = MAX_ALIAS_SEQUENCE_LENGTH,
    .parse_table = &ts_parse_table[0][0],
    .small_parse_table = ts_small_parse_table,
    .small_parse_table_map = ts_small_parse_table_map,
    .parse_actions = ts_parse_actions,
    .symbol_names = ts_symbol_names,
    .field_names = ts_field_names,
    .field_map_slices = ts_field_map_slices,
    .field_map_entries = ts_field_map_entries,
    .symbol_metadata = ts_symbol_metadata,
    .public_symbol_map = ts_symbol_map,
    .alias_map = ts_non_terminal_alias_map,
    .alias_sequences = &ts_alias_sequences[0][0],
    .lex_modes = ts_lex_modes,
    .lex_fn = ts_lex,
    .primary_state_ids = ts_primary_state_ids,
  };
  return &language;
}
#ifdef __cplusplus
}
#endif
