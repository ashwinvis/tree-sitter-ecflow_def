module.exports = grammar({
  name: "ecflow_def",

  extras: $ => [
    /[ \t\n]/,  // whitespace
    $.comment,  // can appear anywhere
  ],

  rules: {
    // TODO: add the actual grammar rules
    source_file: $ => repeat($._definition),

    _definition: $ => choice(
      $.extern_statement,
      field('body', $.suite_definition)
      // TODO: other kinds of definitions
    ),

    extern_statement: $=> seq(
      'extern',
      $.identifier,
    ),

    suite_definition: $ => seq(
      'suite',
      $.identifier,
      field('body', repeat($._suite_block)),
      'endsuite'
    ),

    _suite_block: $=> choice(
      $.defstatus_statement,
      $.edit_statement,
      $.family_definition,
    ),

    defstatus_statement: $ => seq(
      'defstatus',
      $.identifier
    ),

    edit_statement: $ => seq(
      'edit',
      // TODO: only $.edit_type,
      choice($.edit_type, $.identifier),
      repeat($._expression),
    ),

    family_definition: $ => seq(
      'family',
      $.identifier,
      field('preamble', repeat($._family_preamble)),
      field('body', repeat($._family_block)),
      'endfamily'
    ),

    _family_preamble: $=> choice(
      $.trigger_statement,
      $.event_statement,
      $.edit_statement,
      $.repeat_statement,
    ),

    _family_block: $=> choice(
      $.family_definition,
      $.task_definition,
    ),

    event_statement: $=> seq(
      'event',
      repeat($._expression)
    ),

    repeat_statement: $=> seq(
      'repeat',
      repeat($._expression)
    ),

    task_definition: $=> seq(
      'task',
      $.identifier,
      field('body', repeat($._task_block)),
    ),

    _task_block: $=> choice(
      $.trigger_statement,
      $.edit_statement,
      $.date_statement,
      $.cron_statement,
    ),

    trigger_statement: $=> seq(
      'trigger',
      repeat($._expression)
    ),

    date_statement: $=> seq(
      'date',
      repeat(
        choice($.number, '.')
      )
    ),

    cron_statement: $=> seq(
      'cron',
      repeat(
        choice(
          $.cron_option,
          $.time_type
        )
      )
    ),


    edit_type: $ => choice(
      'CLEAREVENT',
      'COMMAND',
      'DOC_PATH',
      'ECF_HOME',
      'ECF_INCLUDE',
      'MODE',
      'NODE',
      'OWNER',
      'PARM',
      'REQUEUEONCOMPLETE',
      'REQUEUEONERROR',
      'SCRIPT_NAME',
      'SCRIPT_PATH',
      'SETEVENT',
      'TASK_TIMEOUT',
      'TASK_TYPE',
      // TODO: other kinds of types
    ),


    // TODO: this is a greedy choice for some statements like event etc. Needs a limited variant without operators
    _expression: $ => choice(
      $.identifier,
      $.boolean_operator,
      $.comparison_operator,
      // $.date_type,
      $.time_type,
      $.string_type,
      $.number,
      // TODO: other kinds of expressions
    ),

    identifier: $ => /[a-zA-Z:_\/\-]+[a-zA-Z:_\/\-\d]*/,  // non whitespace, non quote character, does not begin with a number \d

    string_type: $ => /[\'\"].*[\'\"]/,

    // date_type: $=> /[d]+\.[\d]+.[\d]+/,

    cron_option: $=> /-(w|d|m) [\d,]+/,

    time_type: $=> /[\d]+:[\d]+/,

    boolean_operator: $ => choice(
      'and',
      'or'
    ),

    comparison_operator: $ => choice(
      '<',
      '==',
      '>'
    ),

    number: $ => /\d+/,

    comment: $ => token(seq('#', /.*/)),

    // comment: $ => token(prec(-10, /\#.*\n/))

  }

})
