# tree-sitter-ecflow_def

Tree-sitter syntax grammar for ECflow definition files

## Development

```sh
npm install
source activate.sh
tree-sitter generate
tree-sitter test
# repeat
```

## Usage in neovim

Install `nvim-treesitter` and in neovim's `init.lua` file, configure

```lua
local parser_configs = require('nvim-treesitter.parsers').get_parser_configs()

parser_configs.ecflow_def = {
  install_info = {
    url = "https://codeberg.org/ashwinvis/tree-sitter-ecflow_def.git",
    files = {"src/parser.c"},
    branch = "main",
  },
  filetype = 'def'
}
```

Then, manually clone this repository and copy the queries directory as shown below:

```sh
git clone "https://codeberg.org/ashwinvis/tree-sitter-ecflow_def.git"
rsync -aP ./tree-sitter-ecflow_def/queries/ ~/.config/nvim/after/queries/ecflow_def/
```

Launch `nvim` and execute `:TSinstall ecflow_def`

To debug, execute `:checkhealth nvim-treesitter`.
